(function () {
    'use strict';
    angular
        .module('App')
    .controller('http.Controller', httpController);
    
    function httpController($scope, $http, $log) {
        $http.get('http://localhost:2021/api/Users')
        .success(function (result) {
            $scope.rules = result;
            $log.info(result);
        })
        .error(function (data, status) {
            $log.error(data);
        });
    }
})();