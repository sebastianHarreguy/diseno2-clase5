(function () {
    'use strict';
    angular
        .module('App')
    .controller('XmlHttpRequest.Controller', XmlHttpRequestController);
    
    function XmlHttpRequestController($scope, $filter, $log) {

        $scope.handle = '';

        $scope.lowercasehandle = function () {
            return $filter('lowercase')($scope.handle);
        };

        $scope.characters = 5;

        var rulesrequest = new XMLHttpRequest();
        rulesrequest.onreadystatechange = function () {
            
                if (rulesrequest.readyState == 4 && rulesrequest.status == 200) {
                    $scope.rules = JSON.parse(rulesrequest.responseText);
                    $log.info($scope.rules);
                }
        }
        rulesrequest.open("GET", "http://localhost:2021/api/Users", true);
        rulesrequest.send();
    }
    
})();