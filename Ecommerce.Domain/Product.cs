﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Entities
{
    public class Product
    {
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public virtual ICollection<Image> Images { get; set; }
        public virtual ICollection<Video> Videos { get; set; }

        public virtual ICollection<Feature> Features { get; set; }
        public Product()
        {
            Images = new List<Image>();
            Videos = new List<Video>();
            Features = new List<Feature>();
        }
    }
}
