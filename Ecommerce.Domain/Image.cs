﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Entities
{
    public class Image
    {
        public int ImageID { get; set; }
        public string ImageLink { get; set; }
        public Image()
        {

        }
    }
}
